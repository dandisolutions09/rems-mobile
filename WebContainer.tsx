import React, { useRef, useState } from 'react';
import { ActivityIndicator, ScrollView, RefreshControl, View } from 'react-native';
import RNWebView, {

  WebViewNavigation,
} from 'react-native-webview';
import { TWebContainer } from './constraints';
import { useCustomTheme } from 'app/hooks/useCustomTheme';
//import styles from './styles';

const WebContainer: React.FC<TWebContainer> = props => {
  const webViewRef = useRef<RNWebView | null>(null);
  const [refreshing, setRefreshing] = useState(false);
  const [refresherEnabled, setEnableRefresher] = useState(true);


  const url = props.url;
  const { colors } = useCustomTheme();

  const triggerRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };

  React.useEffect(() => {
    if (refreshing) {
      triggerRefresh();
    }
  }, [refreshing]);

  const Spinner = () => (
    <View >
      <ActivityIndicator size="small" color={colors.primary} />
    </View>
  );

  const onLoadingError = (error: any) => {
    props.onError && props.onError(error.nativeEvent.description);
  };

  const handleHttpError = (error: any) => {
    props.onError && props.onError(error.nativeEvent.description);
  };

  const onNavigationStateChange = (state: WebViewNavigation) => {
    props.onUrlChange && props.onUrlChange(state.url);
  };
  
  const handleScroll = (event:any) => {
    const yOffset = Number(event.nativeEvent.contentOffset.y)
    if (yOffset === 0) {
      setEnableRefresher(true)
    } else {
      setEnableRefresher(false)
    }
  }

  return (
    <View>
      <ScrollView style={{}}
        contentContainerStyle={{ flex: 1 }}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            enabled={refresherEnabled}
            onRefresh={() => {
              triggerRefresh();
              webViewRef?.current?.reload(); // Use optional chaining
            }}
          />
        }>
        <RNWebView
          onScroll={handleScroll}
          ref={webViewRef}
          scalesPageToFit
          javaScriptEnabled={true}
          domStorageEnabled={true}
          setSupportMultipleWindows={false}
          pullToRefreshEnabled={true}
          cacheEnabled={props.cacheEnabled}
          cacheMode="LOAD_DEFAULT"
          source={{ uri: url }}
          startInLoadingState={true}
          renderLoading={Spinner}
          onError={onLoadingError}
          onHttpError={handleHttpError}
          onNavigationStateChange={onNavigationStateChange}
        />
      </ScrollView>
    </View>
  );
};

export default WebContainer;