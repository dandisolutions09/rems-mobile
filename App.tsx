// import React from "react";
// import {
//   ScrollView,
//   StyleSheet,
//   Text,
//   SafeAreaView,
//   Dimensions,
// } from "react-native";
// //import Constants from 'expo-constants';
// import { RefreshControl } from "react-native-web-refresh-control";
// import { WebView } from "react-native-webview";

// export default function App() {
//   const [refreshing, setRefreshing] = React.useState(false);
//   const [lines, setLines] = React.useState([]);

//   const reloadLines = React.useCallback(() => {
//     setRefreshing(true);

//     wait(2000).then(() => {
//       setRefreshing(false);
//       setLines(shuffle(LOREM_IPSUM.split(".")));
//     });
//   }, []);

//   React.useEffect(() => {
//     reloadLines();
//   }, [reloadLines]);

//   return (
//     <SafeAreaView style={styles.container}>
//       <ScrollView
//         refreshControl={
//           <RefreshControl refreshing={refreshing} onRefresh={reloadLines} />
//         }
//       ></ScrollView>

//       {reloadLines && (
//         <WebView
//           source={{ uri: "https://reactnative.dev/" }}
//           style={{ height: Dimensions.get("window").height }}
//         />
//       )}
//     </SafeAreaView>
//   );
// }

// function wait(timeout) {
//   return new Promise((resolve) => {
//     setTimeout(resolve, timeout);
//   });
// }
// function shuffle(array) {
//   var currentIndex = array.length,
//     temporaryValue,
//     randomIndex;

//   // While there remain elements to shuffle...
//   while (0 !== currentIndex) {
//     // Pick a remaining element...
//     randomIndex = Math.floor(Math.random() * currentIndex);
//     currentIndex -= 1;

//     // And swap it with the current element.
//     temporaryValue = array[currentIndex];
//     array[currentIndex] = array[randomIndex];
//     array[randomIndex] = temporaryValue;
//   }

//   return array;
// }

// const styles = StyleSheet.create({
//   container: {
//      height: Dimensions.get('window').height,

//     //height: 400,
//     //paddingTop: Constants.statusBarHeight,
//    // backgroundColor: "lightblue",
//     userSelect: "none",
//   },
// });

import React from "react";
import {
  SafeAreaView,
  StyleSheet,
  Dimensions,
  RefreshControl,
  ScrollView,
} from "react-native";
import { WebView } from "react-native-webview";
import RNRestart from "react-native-restart"; // Import RNRestart

export default function App() {
  const [refreshing, setRefreshing] = React.useState(false);
  const webViewRef = React.useRef();

  const reloadWebView = React.useCallback(() => {
    setRefreshing(true);

    // Simulating a delay before stopping the refreshing state
    setTimeout(() => {
      setRefreshing(false);
      // window.location.reload();
      webViewRef.current.reload();
    }, 2000);
  }, []);

  const handleRefresh = () => {
    reloadWebView();
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView
        contentContainerStyle={styles.scrollView}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={handleRefresh} />
        }
      >
        <WebView
          ref={(ref) => (webViewRef.current = ref)}
          source={{ uri: "https://rems-frontend.onrender.com/" }}
          style={styles.webview}
        />
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollView: {
    flex: 1,
  },
  webview: {
    flex: 1,
  },
});
