import {useEffect, useState} from 'react';
import io from 'socket.io-client';

const useSocket = serverUrl => {
  const [socket, setSocket] = useState(null);

  useEffect(() => {
    // Create a new Socket.io connection when the component mounts
    const newSocket = io(serverUrl);

    // Set the socket state
    setSocket(newSocket);

    // Clean up the socket connection when the component unmounts
    return () => {
      newSocket.disconnect();
    };
  }, [serverUrl]);

  const sendMessage = (eventName, data) => {
    if (socket) {
      socket.emit(eventName, data);
    } else {
      console.error('Socket is not connected');
    }
  };

  const subscribeToEvent = (eventName, callback) => {
    if (socket) {
      socket.on(eventName, callback);
    } else {
      console.error('Socket is not connected');
    }
  };

  return {
    socket,
    sendMessage,
    subscribeToEvent,
  };
};

export default useSocket;
